@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Price Calculation') }}</div>

                    <div class="card-body">
                        <main-page-component></main-page-component>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
