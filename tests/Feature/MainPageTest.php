<?php

namespace Tests\Feature;

use Tests\TestCase;

class MainPageTest extends TestCase
{
    public function test_main_page_display_correctly(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee('Price Calculation');
        $response->assertDontSee('Laravel');
    }

    public function test_main_page_get_all_delivers_correctly(): void
    {
        $response = $this->get('api/v1/get/delivers');

        $response->assertStatus(200);
        $response->assertJsonPath('deliveryCompanies.TransCompany', 'Trans Company');
    }
}
