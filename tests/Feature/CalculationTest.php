<?php

namespace Tests\Feature;

use Tests\TestCase;

class CalculationTest extends TestCase
{
    public function test_calculation_successfully_with_pack_group(): void
    {
        $response = $this->postJson('/api/v1/calculate/cost', [
            'distance' => 10,
        ]);

        $response->assertStatus(422);

        $response = $this->postJson('/api/v1/calculate/cost', [
            'distance' => 10,
            'weight' => 10,
            'deliver' => 'PackGroup',
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment(['totalPrice' => 20]);
    }

    public function test_calculation_successfully_with_trans_company(): void
    {
        $response = $this->postJson('/api/v1/calculate/cost', [
            'distance' => 10,
            'weight' => 'ten',
            'deliver' => 'Trans Company',

        ]);

        $response->assertStatus(422);

        $response = $this->postJson('/api/v1/calculate/cost', [
            'distance' => 10,
            'weight' => 15,
            'deliver' => 'TransCompany',
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment(['totalPrice' => 100]);
    }
}
