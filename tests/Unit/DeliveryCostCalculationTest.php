<?php

namespace Tests\Unit;

use App\DeliveryProviders\DefaultProviderDelivery;
use App\Services\DeliveryCostCalculatorService;
use PHPUnit\Framework\TestCase;

class DeliveryCostCalculationTest extends TestCase
{
    public function test_calculate_cost_depending_on_the_deliver_successful(): void
    {
        $deliver = new DefaultProviderDelivery();
        $result = (new DeliveryCostCalculatorService())->calculateCost(10, 10, $deliver);

        $this->assertEquals(20, $result);
    }
}
