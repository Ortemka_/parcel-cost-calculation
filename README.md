## About the price calculator

One-page website for calculating the cost of parcel delivery Indicate the weight of the parcel, distance (is an
additional parameter to make the calculations a little more complicated) and a delivery service. After which the "
Calculate Price" button will be unlocked. <br>
> _From the task description I decided not to create a database_ <br>
> _Also added PHPUnit tests_

## How to add a new Delivery

- In folder `App\DeliveryProviders` create a new class `(The spelling of the class name is important !!!)` <br>
_Example: company name - Nova Poshta, Class name - NovaPoshtaDelivery._
- Implements it from the `DeliveryInterface` in the folder `App\Contracts`.
- Implement a method for calculating the cost of delivering a package.
- In the controller `DeliveryController` (in the folder `App\Http\Controllers\Api\V1`) add a new delivery to the
  property `$deliveryCompanies` <br>
 _Example: company name - Nova Poshta, means we add `'UkrPoshta' => 'Ukr Poshta',`._
- That's all. Now our delivery is shown in the list.
