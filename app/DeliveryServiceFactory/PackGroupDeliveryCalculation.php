<?php

namespace App\DeliveryServiceFactory;

use App\Contracts\DeliveryInterface;
use App\DeliveryProviders\PackGroupDelivery;

class PackGroupDeliveryCalculation extends DeliveryCalculationFactory
{
    public function getDeliveryService(): DeliveryInterface
    {
        return new PackGroupDelivery();
    }
}
