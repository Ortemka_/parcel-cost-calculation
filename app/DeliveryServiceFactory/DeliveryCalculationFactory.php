<?php

namespace App\DeliveryServiceFactory;

use App\Contracts\DeliveryInterface;

abstract class DeliveryCalculationFactory
{
    abstract public function getDeliveryService(): DeliveryInterface;

    public function calculateDeliveryCost(int $distance, int $weight): int
    {
        $deliveryService = $this->getDeliveryService();

        return $deliveryService->calculateDeliveryCost($distance, $weight);
    }
}
