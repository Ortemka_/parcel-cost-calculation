<?php

namespace App\DeliveryServiceFactory;

use App\Contracts\DeliveryInterface;
use App\DeliveryProviders\DefaultProviderDelivery;

class DefaultProviderDeliveryCalculation extends DeliveryCalculationFactory
{
    public function getDeliveryService(): DeliveryInterface
    {
        return new DefaultProviderDelivery();
    }
}
