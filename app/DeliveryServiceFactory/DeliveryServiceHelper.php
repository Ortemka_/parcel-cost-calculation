<?php

namespace App\DeliveryServiceFactory;

class DeliveryServiceHelper
{
    public static function getCalculationFactory(string $deliveryName): DeliveryCalculationFactory
    {
        return match ($deliveryName) {
            'TransCompany' => new TransCompanyDeliveryCalculation(),
            'PackGroup' => new PackGroupDeliveryCalculation(),
            default => new DefaultProviderDeliveryCalculation(),
        };
    }
}
