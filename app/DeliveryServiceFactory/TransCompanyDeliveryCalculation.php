<?php

namespace App\DeliveryServiceFactory;

use App\Contracts\DeliveryInterface;
use App\DeliveryProviders\TransCompanyDelivery;

class TransCompanyDeliveryCalculation extends DeliveryCalculationFactory
{
    public function getDeliveryService(): DeliveryInterface
    {
        return new TransCompanyDelivery();
    }
}
