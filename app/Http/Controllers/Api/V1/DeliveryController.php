<?php

namespace App\Http\Controllers\Api\V1;

use App\DeliveryServiceFactory\DeliveryServiceHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Delivery\CalculateRequest;

use function response;

class DeliveryController extends Controller
{
    private array $deliveryCompanies = [
        'TransCompany' => 'Trans Company',
        'PackGroup' => 'Pack Group',
    ];

    public function calculateDeliveryCost(CalculateRequest $request)
    {
        $data = $request->validated();

        $distance = $data['distance'];
        $weight = $data['weight'];
        $deliverCompany = $data['deliver'];

        $totalPrice = DeliveryServiceHelper::getCalculationFactory($deliverCompany)
            ->calculateDeliveryCost($distance, $weight);

        return response()->json(['totalPrice' => $totalPrice]);
    }

    public function getDeliveryCompanies()
    {
        return response()->json(['deliveryCompanies' => $this->deliveryCompanies]);
    }
}
