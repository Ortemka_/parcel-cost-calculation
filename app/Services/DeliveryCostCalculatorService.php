<?php

namespace App\Services;

use App\Contracts\DeliveryInterface;

class DeliveryCostCalculatorService
{
    public function calculateCost($distance, $weight, DeliveryInterface $deliveryProvider)
    {
        return $deliveryProvider->calculateDeliveryCost($distance, $weight);
    }
}
