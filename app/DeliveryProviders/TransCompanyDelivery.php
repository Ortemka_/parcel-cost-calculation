<?php

namespace App\DeliveryProviders;

use App\Contracts\DeliveryInterface;

class TransCompanyDelivery implements DeliveryInterface
{
    public function calculateDeliveryCost($distance, $weight): int
    {
        // Up to 10 kg the price will be 10 euros, everything above will cost 100 euros
        return ($weight > 10) ? 100 : 10;
    }
}
