<?php

namespace App\DeliveryProviders;

use App\Contracts\DeliveryInterface;

class DefaultProviderDelivery implements DeliveryInterface
{
    public function calculateDeliveryCost($distance, $weight): int
    {
        // The price will be two euros per kilogram
        return $weight * 2;
    }
}
