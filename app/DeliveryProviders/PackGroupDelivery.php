<?php

namespace App\DeliveryProviders;

use App\Contracts\DeliveryInterface;

class PackGroupDelivery implements DeliveryInterface
{
    public function calculateDeliveryCost($distance, $weight): int
    {
        // The price will be 2 euros per kilogram and kilometer
        return $weight + $distance;
    }
}
