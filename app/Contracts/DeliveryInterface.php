<?php

namespace App\Contracts;

interface DeliveryInterface
{
    public function calculateDeliveryCost($distance, $weight): int;
}
